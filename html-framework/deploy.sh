#! /usr/bin/bash
echo "Stopping server..."
pkill /root/.cargo/bin/uiua
echo "Checking for changes"
git reset --hard HEAD && git pull
echo "Starting server"
/root/.cargo/bin/uiua main.ua